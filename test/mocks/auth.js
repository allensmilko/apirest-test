const faker = require('faker');
const AuthMock = module.exports;

AuthMock.createUser = () => ({
    username: faker.name.findName(),
    email: faker.internet.email(),
    password: faker.internet.password()
})
