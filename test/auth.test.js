const dbHandler = require('./db-handler');
const Service = require('../components/auth/services');
const mock = require('./mocks/auth');

beforeAll(async () => await dbHandler.connect());

afterEach(async () => await dbHandler.clearDatabase());

afterAll(async () => await dbHandler.closeDatabase());

describe('Auth  tests', () => {

    it('should create user correctly', async () => {
        const newUser = mock.createUser();
        expect(async () => await Service.newUser(newUser))
            .not
            .toThrow();
    });
});

