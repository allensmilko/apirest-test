const moment = require('moment');
const mongoose = require("mongoose");
const dbConfig = require('../config/db-connection');
const responseMessages = require('../enums/response-messages');
const middleware = {};

const FailOver = new mongoose.Schema(
 {
  date: { type: Date },
  path: { type: String }
 });

middleware.registerFailover = async(req,res,next) => {
    const { originalUrl } = req;
    const connection = await dbConfig.connectToDatabase();
    let model = connection.model('FailOver',FailOver);

    const failover = new model({
        date: moment().unix(),
        path: originalUrl
    })
    await failover.save();
	next();
}


middleware.returnFailovers = async(req,res,next) => {
    const connection = await dbConfig.connectToDatabase();
    let model = connection.model('FailOver',FailOver);
    await model.find({})
    .exec((errortoFind, failoversFinded) => {
        if(errortoFind){
            const errorMessage = responseMessages.internalError(errortoFind)
            res.status(errorMessage.status).send(errorMessage);
        }
        const responseStatus = responseMessages.ok(failoversFinded)
            res.status(responseStatus.status).send(responseStatus);
    });
}

module.exports = middleware;