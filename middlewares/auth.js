const jwt = require('jwt-simple');
const moment = require('moment');
const secret = require('../config/token').TOKEN_SECRET;
const middleware = {};

middleware.isAuth = (req,res,next) => {
	const token = req.headers.authorization? req.headers.authorization.replace(/['"]+/g,''): "";
	
	if (!req.headers.authorization) {
		return res.status(403).send({message:"No estas logeado"});
	}

	try{
		var payload = jwt.decode(token,secret);
		if (payload.exp <= moment().unix()) {
			return res.status(401).send({message:"El token ha caducado"});
		}
	}catch(ex){
		return res.status(401).send({message:"El token no es valido"});
	}
	req.user = payload
	next();
}

module.exports = middleware;