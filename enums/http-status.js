module.exports = {
    NOT_FOUND: {  
        status: 404,
        message: "Documento no encontrado"
    },
    UNAUTHORIZED: {
        status: 401,
        message: "No estas autorizado para hacer esta peticion"
    },
    DISRUPTED: {
        status: 403,
        message: "Operacion interrumpida"
    },
    OK: {
        status: 200,
        message: "Ok"
    },
    CREATED: {
        status: 201,
        message: "Documento creado con exito"
    },
    INTERNAL_ERROR: {
        status: 500,
        message: "Error interno en el servidor"
    },
};  