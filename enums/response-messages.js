const responseMessages = module.exports;
const { 
    NOT_FOUND, 
    UNAUTHORIZED,
    DISRUPTED,
    OK, 
    CREATED, 
    INTERNAL_ERROR } = require('./http-status'); 
    
responseMessages.notFound = () => ({ ...NOT_FOUND });
responseMessages.disrupted = message => ({ ...DISRUPTED, message });
responseMessages.unauthorized = error => ({ ...UNAUTHORIZED, error });
responseMessages.ok = data => ({ ...OK, data });
responseMessages.created = data => ({ ...CREATED, data });
responseMessages.internalError = error => ({ ...INTERNAL_ERROR, error });
