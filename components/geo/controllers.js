const Service = require('./services');
const Controller = module.exports;

Controller.newCity = (req,res,next) => {
	const { body } = req; 
	Service.newCity(body)
		.then(result => {
			res.status(result.status).send(result);
		})
		.catch(err => {
			res.status(err.status).send(err);
		});
};

Controller.getCities = (req,res,next) => {
	let { query: { perPage = 10 , page = 0} } = req; 
	if(parseInt(page) > 0){
		page = page - 1;
	};
	Service.getCities(perPage, page)
		.then(result => {
			res.status(result.status).send(result);
		})
		.catch(err => {
			res.status(err.status).send(err);
		});
};

Controller.getNearbyCities = (req,res,next) => {
	let { query: { perPage = 10 , page = 0}, params: { longitude, latitude } } = req; 
	if(parseInt(page) > 0){
		page = page - 1;
	};
	Service.getNearbyCities({perPage, page, longitude, latitude})
		.then(result => {
			res.status(result.status).send(result);
		})
		.catch(err => {
			res.status(err.status).send(err);
		});
};