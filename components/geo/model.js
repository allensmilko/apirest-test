
const mongoose = require("mongoose");
const CitySchema = new mongoose.Schema(
 {
  name: String,
  location: {
   type: { type: String },
   coordinates: []
  }
 });
CitySchema.index({ location: "2dsphere" });
module.exports.City = CitySchema;