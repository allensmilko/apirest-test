const City = require('./model').City;
const dbConfig = require('../../config/db-connection');
const responseMessages = require('../../enums/response-messages');
const service = {};

service.newCity = async(data) => {
    const { name , latitude, longitude} = data;
    const connection = await dbConfig.connectToDatabase();
    let model = connection.model('City',City);
    let city = new model({
      name,
      location: {
        type: "Point",
       coordinates: [longitude, latitude]
      }
    });
  return new Promise((resolve, reject) => city.save((errorToSave, citySaved) => errorToSave?
     reject(responseMessages.internalError(errorToSave)) : resolve(responseMessages.ok(citySaved))));
};

service.getCities = async(perPage, page) => {
  const connection = await dbConfig.connectToDatabase();
  let model = connection.model('City',City);
  return new Promise((resolve, reject) => model.find({})
    .limit(parseInt(perPage))
    .skip(parseInt(perPage) * parseInt(page))
    .exec((errortoFind, citiesFinded) => errortoFind?
      reject(responseMessages.internalError(errortoFind)) : resolve(responseMessages.ok(citiesFinded))));
  };

const resolveCities = cities => cities.map(city => {
  const { name, location: {coordinates } } = city;
  return ({name, coordinates});
});

service.getNearbyCities = async(options) => {
  const { perPage, page, longitude, latitude } = options;
  const lng = parseFloat(longitude);
  const lat = parseFloat(latitude);
  const connection = await dbConfig.connectToDatabase();
  let model = connection.model('City',City);
  return new Promise((resolve, reject) => model.find({
    location: {
      $near: {
       $maxDistance: 1000000,
       $geometry: {
        type: "Point",
        coordinates: [lng, lat]
       }
      }
     }
  })
    .select("-__v -_id")
    .limit(parseInt(perPage))
    .skip(parseInt(perPage) * parseInt(page))
    .exec((errortoFind, citiesFinded) => errortoFind?
      reject(responseMessages.internalError(errortoFind)) : resolve(responseMessages
        .ok(resolveCities(citiesFinded))))
  );
  };

module.exports = service;

