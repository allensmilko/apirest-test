const Service = require('./services');

const Controller = module.exports;

Controller.checkemail = (req,res,next) => {
	const { params: {email} } = req; 
	Service.checkemail(email)
		.then(result => {
			res.status(result.status).send(result);
		})
		.catch(err => {
			res.status(err.status).send(err);
		});
};


Controller.me = (req,res,next) => {
	const { user: {sub} } = req; 
	Service.me(sub)
		.then(result => {
			res.status(result.status).send(result);
		})
		.catch(err => {
			res.status(err.status).send(err);
		});
};

Controller.newUser = (req,res,next) => {
	const { body } = req; 
	Service.newUser(body)
		.then(result => {
			res.status(result.status).send(result);
		})
		.catch(err => {
			res.status(err.status).send(err);
		});
};

Controller.updateMe = (req,res,next) => {
	const { user: {sub}, body } = req; 
	Service.updateMe(sub, body)
		.then(result => {
			res.status(result.status).send(result);
		})
		.catch(err => {
			res.status(err.status).send(err);
		});
};

Controller.login = (req,res,next) => {
	const { body } = req; 
	Service.login(body)
		.then(result => {
			res.status(result.status).send(result);
		})
		.catch(err => {
			res.status(err.status).send(err);
		});
};
