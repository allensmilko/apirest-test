const mongoose = require('mongoose');

const UserSchema = new mongoose.Schema({
	username: { type:String },
	email: { type:String },
	password: { type:String },
	lats_update: { type: Date },
	created_at: { type: Date },
});

module.exports.User = UserSchema;