const User = require('./model').User;
const moment = require('moment');
const bcrypt = require('bcrypt-nodejs');
const authservice = require('../../services/auth');
const dbConfig = require('../../config/db-connection');
const responseMessages = require('../../enums/response-messages');
const service = module.exports;

service.checkemail = async(data) => {
  let connection = await dbConfig.connectToDatabase();
  let model = connection.model('User',User);
  return new Promise((resolve, reject) => {
    model.findOne({email:data.toLowerCase()}, function(err, userFinded) {
      if (err) {
          return reject(responseMessages.internalError(err));
      } else {
         if(userFinded){
          return resolve(responseMessages.ok(userFinded));
         }
        else{
          return reject(responseMessages.notFound());
        }
      };
    });
  });
};

service.newUser = async(data) => {
  const { username, email, password } = data;
  let connection = await dbConfig.connectToDatabase();
  let model = connection.model('User',User);
  let user = new model({
    username : username,
    email: email,
    password: password,
    lats_update: moment().unix(),
    created_at: moment().unix()
  });
   return new Promise((resolve, reject) => {
    bcrypt.hash(password.toLowerCase(),null,null,(err,hash) => {
      if(err){
        return reject(responseMessages.internalError(err));
      }
      else{
          user.password = hash;
          model.findOne({email},(err,userfinded) => {
            if (userfinded) {
              return reject(responseMessages.disrupted({disrupted_error: "Este usuario ya existe"}));
            }
            else if(err){
              return reject(responseMessages.internalError(err));
            }
            else{
                user.save((err,usercreated) => {
                  if (err) {
                      return eject(responseMessages.internalError(err));
                  }
                  else{
                    const dataResponse = {
                      token: authservice.createToken(usercreated),
                      user: usercreated
                    } ;
                    return resolve(responseMessages.ok(dataResponse));
                  }
                })
              }
            })
      }
    })
   });
}

service.login = async(data) => {
  const { email, password } = data;
  let connection = await dbConfig.connectToDatabase();
  let model = connection.model('User',User);
  return new Promise(function(resolve, reject){
    model.findOne({email: email.toLowerCase()}, (err, userFinded) => {
            if (err) {
                return reject(responseMessages.internalError(err));
            } else {
               if(userFinded){
                 bcrypt.compare(password,userFinded.password,(err,check) => {
                    if(check){
                      const dataResponse = {
                        token: authservice.createToken(userFinded)
                      };
                      return resolve(responseMessages.ok(dataResponse));
                    }
                    else{
                       return reject(responseMessages.disrupted({disrupted_error: "El usuario no inicio sesion correctamente"}));
                    }
                })
               }
              else{
                return reject(responseMessages.disrupted({disrupted_error: "El usuario no inicio sesion correctamente"}));
              }
          }
        });
      });    
};

service.me = async(userId) => {
  let connection = await dbConfig.connectToDatabase();
  let model = connection.model('User',User);
  return new Promise((resolve, reject) => {
    model.findById(userId)
    .select('-password')
    .exec((err, user) => {
      if (err) {
          return reject(responseMessages.internalError(err));
      } else {
         if(user){
          return resolve(responseMessages.ok(user));
         }
        else{
          return reject(responseMessages.notFound(err));
        }
      };
    });
  });
};

service.updateMe = async(userId, data) => {
  let connection = await dbConfig.connectToDatabase();
  let model = connection.model('User',User);
  return new Promise((resolve, reject) => {
    model.findOneAndUpdate({_id: userId}, data, {new:true}, function(err, userUpdated) {
      if (err) {
        return reject(responseMessages.internalError(err));
      } else {
         if(userUpdated){
          const dataResponse = {
            token: authservice.createToken(userUpdated),
            user: userUpdated
          };
          return resolve(responseMessages.ok(dataResponse));
         }
        else{
          return reject(responseMessages.notFound());
        }
      };
    });
  });
}


