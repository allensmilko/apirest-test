'use strict'
const cors = require("cors");
const express = require('express');
const bodyParser = require("body-parser");
const serverless = require('serverless-http');
const Authcontroller = require('./components/auth/controllers');
const GeoController = require('./components/geo/controllers');
const userMiddleware = require('./middlewares/auth');
const failover = require('./middlewares/failover');
const app = express();
	  app.use(cors());
	  app.use(bodyParser.urlencoded({ extended: false }));
	  app.use(bodyParser.json());

	//   AUTH STACK
	  app.get('/api/v1/auth/checkemail/:email', failover.registerFailover, Authcontroller.checkemail);
	  app.post('/api/v1/auth/register', failover.registerFailover, Authcontroller.newUser);
	  app.post('/api/v1/auth/login', failover.registerFailover, Authcontroller.login);
	  app.get('/api/v1/auth/me', failover.registerFailover, userMiddleware.isAuth, Authcontroller.me);
	  app.post('/api/v1/auth/update-me', failover.registerFailover, userMiddleware.isAuth, Authcontroller.updateMe);

	//   GEO STACK 
	  app.post('/api/v1/geo/new-city', failover.registerFailover, GeoController.newCity);
	  app.get('/api/v1/geo/get-cities', failover.registerFailover, GeoController.getCities);
	  app.get('/api/v1/geo/get-nenarby-cities/:longitude/:latitude',  failover.registerFailover, userMiddleware.isAuth, GeoController.getNearbyCities);


	  app.get('/api/v1/failover',  failover.returnFailovers);

module.exports.handler = serverless(app);
