const   mongoose = require('mongoose');
const { MONGO_DB } = process.env;
const DbConnection = module.exports; 
var   cachedDb = null;

DbConnection.connectToDatabase = async() => {
    if (cachedDb == null) {
       return cachedDb = await mongoose.createConnection(MONGO_DB, {
            // Buffering means mongoose will queue up operations if it gets
            // disconnected from MongoDB and send them when it reconnects.
            // With serverless, better to fail fast if not connected.
            bufferCommands: false, // Disable mongoose buffering
            bufferMaxEntries: 0 // and MongoDB driver buffering
          });
    }else{
        return cachedDb;
    }
}