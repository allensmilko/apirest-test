# Api test
esta api tiene un sistema de autenticacion y greacion de puntos por geo referenciacion.

# Funcionalidad!

  - Registrar usuario
  - Validar si un usuario ya esta registrado solo con el E-mailo
  - Login
  - Actualizacion de datos el usuario
  - Traer informacion del usuario
  - Crear ciudades con longitud y latitud (por medio de 2dSphere de mongo que funciona con la formula Haversine https://en.wikipedia.org/wiki/Haversine_formula)
  - Traer ciudades paginadas
  - Traer ciudades mas cercanas al usuario logueado paginadas
  - Llevar registro de todas las peticiones


Markdown is a lightweight markup language based on the formatting conventions that people naturally use in email.  As [John Gruber] writes on the [Markdown site][df1]


### Tecnologias

Se trabajo en un cosistema Amazon con las siguientes tecnologias.

* [Nodejs] 
* [serverless] - framework enfocado en la arquitectura serverless en este caso Lambda
* [jest] - Para pruebas unitarias
* [Express] - Para agilizar el ruteo de la lambda
* [simple jwt] - Para el sistema de autenticacion
* [mongodb] - Base de datos hospedada en un ecosistema aws dentro de mongocloud

## Requisitos:
*   Nodejs v1 o mayor.
*   Serverless instalado ```= npm i -g serverless```
*   visual studio code como debugger

## Proceso de instalacion y puesta a correr.
* Clone el proyecto.
* Dentro de la carpeta del proyecto ejecute ``` npm install ```
* Por ultimo dirijase a la seccion de debugging del vs code y ejecute el debugger.
* Otra opcion es ejecutar ``` sls offline ```.
* En el archivo ``` testapi.json ``` se encuentra la coleccion de postman para poder consumir el servicio.
* Setie las variables de entorno en postman de esta manera
* url: http://localhost:3000, api-version: /api/v1, auth [El token que se genera una vez registra el usuario]

### Por hacer
* Mejorar el failover agregando codigos de respuesta por peticion.
* Agregar un cache layer para empalmar los codigos de respuesta del failover y actualizar el documento.
* realizar pruebas unitarias de integracion y de regresion (No alcanzo el tiempo para realizarlas pero quedo una base sin fixtures).

